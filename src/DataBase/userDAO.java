/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DataBase;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import libraryproject.LibraryProject;

/**
 *
 * @author Asus
 */
public class userDAO {
    public static boolean insert(User user){
        return true;
    }
    public static boolean update (User user){
        return true;
    }
     public static boolean delete (User user){
        return true;
    }
    public static ArrayList<User> getUsers(){
        ArrayList<User> list = new ArrayList();
        Connection conn = Database.connect();
        try {
            Statement stm = conn.createStatement();
            String sql = "SELECT userId,\n" +
                        "       logInName,\n" +
                        "       password,\n" +
                        "       name,\n" +
                        "       surname,\n" +
                        "       typeId\n" +
                        "  FROM user";
            ResultSet rs = stm.executeQuery(sql);
            while(rs.next()){
                System.out.println(rs.getInt("userId") + " " + rs.getString("loginName"));
                User user =  toObject(rs);
                list.add(user);
                
        }
            Database.close();
            return list;
        } catch (SQLException ex) {
            Logger.getLogger(LibraryProject.class.getName()).log(Level.SEVERE, null, ex);
        }
        Database.close();
        return null;
    }

    private static User toObject(ResultSet rs) throws SQLException {
        User user;
        user = new User();
        user.setUserId(rs.getInt("userId"));
        user.setLoginName(rs.getString("loginName"));
        user.setName(rs.getString("name"));
        user.setSurname(rs.getString("surname"));
        user.setPassword(rs.getString("password"));
        user.setTypeId(rs.getInt("typeId"));
        return user;
    }
     public User getUser(int userId){
        return null;
    }
    
}